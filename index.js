// Add Express
const express = require("express");

// Initialize Express
const app = express();

// Create GET request
app.get("/email", (req, res) => {
  const value = {
    email: [
      { type: "required", message: "L'email est requis" },
      { type: "pattern", message: "Entrer un email valide" },
    ],
  };
  res.json(value);
});

// Initialize server
app.listen(5000, () => {
  console.log("Running on port 5000.");
});

// Export the Express API
module.exports = app;
